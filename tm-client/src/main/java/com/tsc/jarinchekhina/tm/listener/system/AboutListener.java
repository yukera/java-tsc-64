package com.tsc.jarinchekhina.tm.listener.system;

import com.jcabi.manifests.Manifests;
import com.tsc.jarinchekhina.tm.event.ConsoleEvent;
import com.tsc.jarinchekhina.tm.listener.AbstractListener;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public final class AboutListener extends AbstractListener {

    @NotNull
    @Override
    public String arg() {
        return "-a";
    }

    @NotNull
    @Override
    public String name() {
        return "system-about";
    }

    @NotNull
    @Override
    public String description() {
        return "display developer info";
    }

    @Override
    @EventListener(condition = "@aboutListener.name() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("[ABOUT]");
        @NotNull final String developer = Manifests.read("developer");
        System.out.println("Developer: " + developer);
        @NotNull final String email = Manifests.read("email");
        System.out.println("E-mail: " + email);
    }

}
