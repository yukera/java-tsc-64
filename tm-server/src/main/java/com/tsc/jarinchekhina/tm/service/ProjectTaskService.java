package com.tsc.jarinchekhina.tm.service;

import com.tsc.jarinchekhina.tm.api.service.IProjectService;
import com.tsc.jarinchekhina.tm.api.service.IProjectTaskService;
import com.tsc.jarinchekhina.tm.api.service.ITaskService;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyIdException;
import com.tsc.jarinchekhina.tm.exception.user.AccessDeniedException;
import com.tsc.jarinchekhina.tm.model.Project;
import com.tsc.jarinchekhina.tm.model.Task;
import com.tsc.jarinchekhina.tm.util.DataUtil;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    @Autowired
    private IProjectService projectService;

    @NotNull
    @Autowired
    private ITaskService taskService;

    @NotNull
    @Override
    @SneakyThrows
    public Task bindTaskByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(projectId)) throw new EmptyIdException();
        if (DataUtil.isEmpty(taskId)) throw new EmptyIdException();
        @NotNull final Task task = taskService.findById(userId, taskId);
        @NotNull final Project project = projectService.findById(userId, projectId);
        task.setProject(project);
        taskService.update(task);
        return task;
    }

    @Override
    @SneakyThrows
    public void clearProjects(@Nullable final String userId) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        @NotNull final List<Project> projects = projectService.findAll(userId);
        for (@NotNull final Project project : projects) {
            @NotNull String projectId = project.getId();
            taskService.removeAllByProjectId(projectId);
        }
        projectService.clear(userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAllTaskByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(projectId)) throw new EmptyIdException();
        return taskService.findAllByProjectId(userId, projectId);
    }

    @Override
    @SneakyThrows
    public void removeProjectById(@Nullable final String userId, @Nullable final String projectId) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(projectId)) throw new EmptyIdException();
        taskService.removeAllByProjectId(projectId);
        projectService.removeById(userId, projectId);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task unbindTaskByProjectId(@Nullable final String userId, @Nullable final String taskId) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(taskId)) throw new EmptyIdException();
        @NotNull final Task task = taskService.findById(userId, taskId);
        task.setProject(null);
        taskService.update(task);
        return task;
    }

}
