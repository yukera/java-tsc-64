package com.tsc.jarinchekhina.tm.service;

import com.tsc.jarinchekhina.tm.api.service.ITaskService;
import com.tsc.jarinchekhina.tm.enumerated.Status;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyDescriptionException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyIdException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyNameException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyStatusException;
import com.tsc.jarinchekhina.tm.exception.entity.TaskNotFoundException;
import com.tsc.jarinchekhina.tm.exception.entity.UserNotFoundException;
import com.tsc.jarinchekhina.tm.exception.user.AccessDeniedException;
import com.tsc.jarinchekhina.tm.model.Task;
import com.tsc.jarinchekhina.tm.repository.TaskRepository;
import com.tsc.jarinchekhina.tm.repository.UserRepository;
import com.tsc.jarinchekhina.tm.util.DataUtil;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.var;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@AllArgsConstructor
public class TaskService extends AbstractService<Task> implements ITaskService {

    @NotNull
    @Autowired
    public TaskRepository taskRepository;

    @NotNull
    @Autowired
    public UserRepository userRepository;


    @Override
    @Transactional
    public void add(@NotNull final Task task) {
        taskRepository.save(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void add(@Nullable final String userId, @Nullable final Task task) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (task == null) throw new TaskNotFoundException();
        if (!userId.equals(task.getUser().getId())) throw new AccessDeniedException();
        taskRepository.save(task);
    }

    @Override
    @Transactional
    public void addAll(@NotNull final List<Task> list) {
        taskRepository.saveAll(list);
    }

    @Override
    @Transactional
    public void update(@NotNull final Task task) {
        taskRepository.save(task);
    }

    @Override
    @Transactional
    public void clear() {
        taskRepository.deleteAll();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear(@Nullable final String userId) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        taskRepository.deleteAllByUser_Id(userId);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void create(@Nullable final String userId, @Nullable final String name) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        @NotNull final Task task = new Task();
        task.setName(name);
        final var user = userRepository.findById(userId);
        if (!user.isPresent()) throw new UserNotFoundException();
        task.setUser(user.get());
        taskRepository.save(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        if (DataUtil.isEmpty(description)) throw new EmptyDescriptionException();
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        final var user = userRepository.findById(userId);
        if (!user.isPresent()) throw new UserNotFoundException();
        task.setUser(user.get());
        taskRepository.save(task);
    }

    @NotNull
    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAll(@Nullable final String userId) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        return taskRepository.findAllByUser_Id(userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        return taskRepository.findAllByUser_IdAndProject_Id(userId, projectId);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task findById(@NotNull final String id) {
        final var task = taskRepository.findById(id);
        if (!task.isPresent()) throw new TaskNotFoundException();
        return task.get();
    }

    @Override
    @Transactional
    public void remove(@NotNull final Task task) {
        taskRepository.delete(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void remove(@Nullable final String userId, @Nullable final Task task) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (task == null) return;
        if (task.getUser() == null || !userId.equals(task.getUser().getId())) throw new AccessDeniedException();
        taskRepository.delete(task);
    }

    @Override
    @Transactional
    public void removeById(@NotNull final String id) {
        taskRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void removeAllByProjectId(@NotNull final String projectId) {
        taskRepository.deleteAllByProject_Id(projectId);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        final var task = findById(id);
        if (task.getUser() == null || !userId.equals(task.getUser().getId())) throw new AccessDeniedException();
        taskRepository.deleteById(id);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeByName(@Nullable final String userId, @Nullable final String name) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        final var task = findByName(userId, name);
        if (task.getUser() == null || !userId.equals(task.getUser().getId())) throw new AccessDeniedException();
        taskRepository.deleteByName(name);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void updateTaskById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        if (DataUtil.isEmpty(description)) throw new EmptyDescriptionException();
        @NotNull final Task task = findById(userId, id);
        task.setName(name);
        task.setDescription(description);
        taskRepository.save(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task findById(@Nullable final String userId, @Nullable final String id) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        final var task = taskRepository.findById(id);
        if (!task.isPresent()) throw new TaskNotFoundException();
        return task.get();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void startTaskById(@Nullable final String userId, @Nullable final String id) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        changeTaskStatusById(userId, id, Status.IN_PROGRESS);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final Task task = findById(userId, id);
        changeTaskStatus(userId, task, status);
    }

    @SneakyThrows
    @Transactional
    public void changeTaskStatus(
            @NotNull final String userId,
            @NotNull final Task task,
            @NotNull final Status status
    ) {
        task.setStatus(status);
        taskRepository.save(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void startTaskByName(@Nullable final String userId, @Nullable final String name) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        changeTaskStatusByName(userId, name, Status.IN_PROGRESS);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void changeTaskStatusByName(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final Status status
    ) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final Task task = findByName(userId, name);
        changeTaskStatus(userId, task, status);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task findByName(@Nullable final String userId, @Nullable final String name) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        final var task = taskRepository.findFirstByUser_IdAndName(userId, name);
        if (!task.isPresent()) throw new TaskNotFoundException();
        return task.get();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void finishTaskById(@Nullable final String userId, @Nullable final String id) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        changeTaskStatusById(userId, id, Status.COMPLETED);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void finishTaskByName(@Nullable final String userId, @Nullable final String name) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        changeTaskStatusByName(userId, name, Status.COMPLETED);
    }

}
