package com.tsc.jarinchekhina.tm.model;

import com.tsc.jarinchekhina.tm.enumerated.Status;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.UUID;

/**
 * Task
 *
 * @author Yuliya Arinchekhina
 */
@Data
public class Task {

    private String id = UUID.randomUUID().toString();

    private String name;

    private String description;

    private Status status = Status.NOT_STARTED;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateStart;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateFinish;

    private String projectId;

    public Task(String name) {
        this.name = name;
    }

}
