package com.tsc.jarinchekhina.tm.repository;

import com.tsc.jarinchekhina.tm.model.Project;
import lombok.Getter;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * ProjectRepository
 *
 * @author Yuliya Arinchekhina
 */
@Repository
public class ProjectRepository {

    private static final ProjectRepository INSTANCE = new ProjectRepository();

    public static ProjectRepository getInstance() {
        return INSTANCE;
    }

    private Map<String, Project> projects = new LinkedHashMap<>();

    {
        add(new Project("FIRST"));
        add(new Project("SECOND"));
        add(new Project("THIRD"));
    }

    public void create() {
        add(new Project("New Project " + System.currentTimeMillis()));
    }

    public void add(Project project) {
        projects.put(project.getId(), project);
    }

    public void save(Project project) {
        projects.put(project.getId(), project);
    }

    public Collection<Project> findAll() {
        return projects.values();
    }

    public Project findById(String id) {
        return projects.get(id);
    }

    public void removeById(String id) {
        projects.remove(id);
    }

}
